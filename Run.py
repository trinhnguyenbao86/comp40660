from StandardA import StandardA
from StandardG import StandardG
from StandardN import StandardN
from StandardAC_W1 import StandardACW1
from StandardAC_W2 import StandardACW2
from StandardAX import StandardAX

def main():

    Protocol = int(input("Enter protocol: 1. UDP | 2. TCP \n"))

    Standard = int(input("Enter Standard: 1. 802.11a | 2. 802.11g | 3. 802.11n | 4. 802.11ac_w1 | 5. 802.11ac_w2 | 6. 802.11ax \n"))

    if Standard == 1:
        A = StandardA()
        A.GetDataRate()
        if Protocol == 1:
            A.GetThroughput_UDP()
        else:
            A.GetThroughput_TCP()

    elif Standard == 2:
        G = StandardG()
        G.GetDataRate()
        if Protocol == 1:
            G.GetThroughput_UDP()
        else:
            G.GetThroughput_TCP()

    elif Standard == 3:
        N = StandardN()
        N.GetDataRate()
        if Protocol == 1:
            print("Normal Case: ")
            N.GetThroughput_UDP(1)
            print("Best Case: ")
            N.GetThroughput_UDP(4)
        else:
            print("Normal Case: ")
            N.GetThroughput_TCP(1)
            print("Best Case: ")
            N.GetThroughput_TCP(4)

    elif Standard == 4:
        AC1 = StandardACW1()
        AC1.GetDataRate()
        if Protocol == 1:
            print("Normal Case: ")
            AC1.GetThroughput_UDP(1)
            print("Best Case: ")
            AC1.GetThroughput_UDP(3)
        else:
            print("Normal Case: ")
            AC1.GetThroughput_TCP(1)
            print("Best Case: ")
            AC1.GetThroughput_TCP(3)

    elif Standard == 5:
        AC2 = StandardACW2()
        AC2.GetDataRate()
        if Protocol == 1:
            print("Normal Case: ")
            AC2.GetThroughput_UDP(1)
            print("Best Case: ")
            (AC2.GetThroughput_UDP(8))
        else:
            print("Normal Case: ")
            AC2.GetThroughput_TCP(1)
            print("Best Case: " )
            AC2.GetThroughput_TCP(8)

    elif Standard == 6:
        AX = StandardAX()
        AX.GetDataRate()
        if Protocol == 1:
            print("Normal Case: ")
            AX.GetThroughput_UDP(1)
            print("Best Case: ")
            AX.GetThroughput_UDP(8)
        else:
            print("Normal Case: ")
            AX.GetThroughput_TCP(1)
            print("Best Case: ")
            AX.GetThroughput_TCP(8)

if __name__ == '__main__':
    main()






