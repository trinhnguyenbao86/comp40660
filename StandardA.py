import math

class StandardA():
    SIFS = 16   # Microseconds
    SlotTime = 9    # Microseconds
    DIFS = 2*SlotTime + SIFS   # Microseconds
    FrameSize = 1542 # Bytes (including 1500 payload + 34 MAC Header + 8 SNAP LLC Header)
    TCP_ACK = 82 # Bytes (including 40 payload + 34 MAC Header + 8 SNAP LLC Header)
    Preamble_Normal = 20 # Microseconds
    SDur = 4.0  # Symbol Duration (Microsecond)
    RTS = 4.0   # Microseconds (for 1 symbol)
    CTS = 4.0   # Microseconds (for 1 symbol)
    ACK_MAC = 4     # Microseconds

    def __init__(self):
        print("802.11a is chosen !")

    def GetDataRate(self):
        dataRate = int(input("1-> 6 Mbps \n2-> 9 Mbps\n3-> 12 Mbps\n4-> 18 Mbps\n"
              "5-> 24 Mbps\n6-> 36 Mbps\n7-> 48 Mbps\n8-> 54 Mbps\n"))
        self.dataRate = dataRate

    # CALCULATE Bits Per Symbol:
    def BitsPerSymbol(self):
        # Data bits per OFDM Symbol = NBits * CRate * NChan
        # Data Rate =  NBits * CRate * NChan / SDur
        if self.dataRate == 1:       # 6 Mbps
            return 1 * (1/2) * 48
        elif self.dataRate == 2:     # 9 Mbps
            return 1 * (3/4) * 48
        elif self.dataRate == 3:     # 12 Mbps
            return 2 * (1/2) * 48
        elif self.dataRate == 4:     # 18 Mbps
            return 2 * (3/4) * 48
        elif self.dataRate == 5:     # 24 Mbps
            return 4 * (1/2) * 48
        elif self.dataRate == 6:     # 36 Mbps
            return 4 * (3/4) * 48
        elif self.dataRate == 7:     # 48 Mbps
            return 6 * (2/3) * 48
        else:                        # 54 Mbps
            return 6 * (3/4) * 48

    def GetThroughput_UDP(self):    # Calculate total time duration for UDP
        NumberOfBitsPerSymbol = self.BitsPerSymbol()
        dataTime = self.SDur * math.ceil((self.FrameSize*8 + 6) / NumberOfBitsPerSymbol)

        Throughput =  1500.00*8 / (self.DIFS + \
               self.Preamble_Normal + self.RTS + self.SIFS + \
               self.Preamble_Normal + self.CTS + self.SIFS + \
               self.Preamble_Normal  + dataTime + self.SIFS + \
               self.Preamble_Normal + self.ACK_MAC)
        print('Throughput (UDP) = ' + str(round(Throughput, 2)) + ' Mbps')
        print('The amount of time needed to transfer 10GB: ' + str(round(10000000 * 8 / Throughput, 2)))

    def GetThroughput_TCP(self):    # Calculate total time duration for TCP
        NumberOfBitsPerSymbol = self.BitsPerSymbol()
        dataTime = self.SDur * math.ceil((self.FrameSize*8 + 6) / NumberOfBitsPerSymbol)
        TCP_ACK_Time = self.SDur * math.ceil((self.TCP_ACK*8 + 6) / NumberOfBitsPerSymbol)

        Throughput =  1500.0*8 / (self.DIFS + \
               self.Preamble_Normal + self.RTS + self.SIFS + \
               self.Preamble_Normal + self.CTS + self.SIFS + \
               self.Preamble_Normal  + dataTime + self.SIFS + \
               self.Preamble_Normal + self.ACK_MAC + \
               self.DIFS + \
               self.Preamble_Normal + self.RTS + self.SIFS + \
               self.Preamble_Normal + self.CTS + self.SIFS + \
               self.Preamble_Normal + TCP_ACK_Time + self.SIFS + \
               self.Preamble_Normal + self.ACK_MAC)
        print('Throughput (TCP) = ' + str(round(Throughput, 2)) + ' Mbps')
        print('The amount of time needed to transfer 10GB: ' + str(round(10000000 * 8 / Throughput, 2)))
