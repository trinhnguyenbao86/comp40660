import math

class StandardAX():
    SIFS = 16   # Microseconds
    SlotTime = 9    # Microseconds
    DIFS = 2*SlotTime + SIFS  # Microseconds
    FrameSize = 1548 # Bytes (including 1500 payload + 40 MAC Header + 8 SNAP LLC Header)
    TCP_ACK = 88 # Bytes (including 40 payload + 40 MAC Header + 8 SNAP LLC Header)
    Preamble_Normal = 20 # Microseconds
    Preamble_Best = 92.8 # Microseconds
    SDur = 13.6  # Symbol Duration (Microsecond)
    RTS = 3.6   # Microseconds (for 1 symbol)
    CTS = 3.6   # Microseconds (for 1 symbol)
    ACK_MAC = 3.6    # Microseconds

    def __init__(self):
        print("802.11ax is chosen !")

    def GetDataRate(self):
        dataRate = int(input("1-> 8.6 Mbps \n2-> 17.2 Mbps\n3-> 25.7 Mbps\n4-> 34.4 Mbps\n"
              "5-> 51.6 Mbps\n6-> 68.8 Mbps\n7-> 77.4 Mbps\n8-> 86.0 Mbps\n9-> 103.2 Mbps\n"
                             "10-> 114.7.3 Mbps\n11-> 129 Mbps\n 12-> 143.4 Mbps\n"))
        self.dataRate = dataRate

    # CALCULATE Bits Per Symbol:
    def BitsPerSymbol(self, NSS):
        # Data bits per OFDM Symbol = NBits * CRate * NChan
        # Data Rate =  NBits * CRate * NChan / SDur
        if self.dataRate == 1:       # 8.6Mbps
            return 1 * (1/2) * (234 if NSS == 1 else 1960) * NSS
        elif self.dataRate == 2:     # 17.2 Mbps
            return 2 * (1/2) * (234 if NSS == 1 else 1960) * NSS
        elif self.dataRate == 3:     # 25.8 Mbps
            return 2 * (3/4) * (234 if NSS == 1 else 1960) * NSS
        elif self.dataRate == 4:     # 34.4 Mbps
            return 4 * (1/2) * (234 if NSS == 1 else 1960) * NSS
        elif self.dataRate == 5:     # 51.6 Mbps
            return 4 * (3/4) * (234 if NSS == 1 else 1960) * NSS
        elif self.dataRate == 6:     # 68.8 Mbps
            return 6 * (2/3) * (234 if NSS == 1 else 1960) * NSS
        elif self.dataRate == 7:     # 77.4 Mbps
            return 6 * (3/4) * (234 if NSS == 1 else 1960) * NSS
        elif self.dataRate == 8:    # 86.0 Mbps
            return 6 * (5/6) * (234 if NSS == 1 else 1960) * NSS
        elif self.dataRate == 9:    # 103.2 Mbps
            return 8 * (3/4) * (234 if NSS == 1 else 1960) * NSS
        elif self.dataRate == 10:   # 114.7 Mbps
            return 8 * (5/6) * (234 if NSS == 1 else 1960) * NSS
        elif self.dataRate == 11:   # 129 Mbps
            return 10 * (3/4) * (234 if NSS == 1 else 1960) * NSS
        else:                        # 143.4 Mbps
            return 10 * (5/6) * (234 if NSS == 1 else 1960) * NSS

    def GetThroughput_UDP(self, NSS):    # Calculate total time duration for UDP
        NumberOfBitsPerSymbol = self.BitsPerSymbol(NSS)
        dataTime = self.SDur * math.ceil((self.FrameSize*8 + 6) / NumberOfBitsPerSymbol)
        Throughput = 1500*8 / (self.DIFS + \
               (self.Preamble_Normal if NSS == 1 else self.Preamble_Best) + self.RTS + self.SIFS + \
               (self.Preamble_Normal if NSS == 1 else self.Preamble_Best) + self.CTS + self.SIFS + \
               (self.Preamble_Normal if NSS == 1 else self.Preamble_Best) + dataTime + self.SIFS + \
               (self.Preamble_Normal if NSS == 1 else self.Preamble_Best) + self.ACK_MAC)
        print('Throughput (UDP) = ' + str(round(Throughput, 2)) + ' Mbps')
        print('The amount of time needed to transfer 10GB: ' + str(round(10000000 * 8 / Throughput, 2)))


    def GetThroughput_TCP(self, NSS):    # Calculate total time duration for TCP
        NumberOfBitsPerSymbol = self.BitsPerSymbol(NSS)
        dataTime = self.SDur * math.ceil((self.FrameSize*8 + 6) / NumberOfBitsPerSymbol)
        TCP_ACK_Time = self.SDur * math.ceil((self.TCP_ACK*8 + 6) / NumberOfBitsPerSymbol)
        Throughput = 1500*8 / (self.DIFS + \
               (self.Preamble_Normal if NSS == 1 else self.Preamble_Best) + self.RTS + self.SIFS + \
               (self.Preamble_Normal if NSS == 1 else self.Preamble_Best) + self.CTS + self.SIFS + \
               (self.Preamble_Normal if NSS == 1 else self.Preamble_Best) + dataTime + self.SIFS + \
               (self.Preamble_Normal if NSS == 1 else self.Preamble_Best) + self.ACK_MAC + \
               self.DIFS + \
               (self.Preamble_Normal if NSS == 1 else self.Preamble_Best) + self.RTS + self.SIFS + \
               (self.Preamble_Normal if NSS == 1 else self.Preamble_Best) + self.CTS + self.SIFS + \
               (self.Preamble_Normal if NSS == 1 else self.Preamble_Best) + TCP_ACK_Time + self.SIFS + \
               (self.Preamble_Normal if NSS == 1 else self.Preamble_Best) + self.ACK_MAC)
        print('Throughput (TCP) = ' + str(round(Throughput, 2)) + ' Mbps')
        print('The amount of time needed to transfer 10GB: ' + str(round(10000000 * 8 / Throughput, 2)))

