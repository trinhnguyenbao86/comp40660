import math

class StandardN():
    SIFS = 16   # Microseconds
    SlotTime = 9    # Microseconds
    DIFS = 2*SlotTime + SIFS  # Microseconds
    FrameSize = 1548 # Bytes (including 1500 payload + 40 MAC Header + 8 SNAP LLC Header)
    TCP_ACK = 88 # Bytes (including 40 payload + 40 MAC Header + 8 SNAP LLC Header)
    Preamble_Normal = 20 # Microseconds
    Preamble_Best = 46 # Microseconds
    SDur = 3.6  # Symbol Duration (Microsecond)
    RTS = 3.6   # Microseconds (for 1 symbol)
    CTS = 3.6   # Microseconds (for 1 symbol)
    ACK_MAC = 3.6    # Microseconds

    def __init__(self):
        print("802.11n is chosen !")

    def GetDataRate(self):
        dataRate = int(input("1-> 7.2 Mbps \n2-> 14.4 Mbps\n3-> 21.7 Mbps\n4-> 28.9 Mbps\n"
              "5-> 43.3 Mbps\n6-> 57.8 Mbps\n7-> 65 Mbps\n8-> 72.2 Mbps\n"))
        self.dataRate = dataRate

    # CALCULATE Bits Per Symbol:
    def BitsPerSymbol(self, NSS):
        # Data bits per OFDM Symbol = NBits * CRate * NChan
        # Data Rate =  NBits * CRate * NChan / SDur
        if self.dataRate == 1:       # 7.2Mbps
            return 1 * (1/2) * (52 if NSS == 1 else 108) * NSS
        elif self.dataRate == 2:     # 14.4 Mbps
            return 2 * (1/2) * (52 if NSS == 1 else 108) * NSS
        elif self.dataRate == 3:     # 21.7 Mbps
            return 2 * (3/4) * (52 if NSS == 1 else 108) * NSS
        elif self.dataRate == 4:     # 28.9 Mbps
            return 4 * (1/2) * (52 if NSS == 1 else 108) * NSS
        elif self.dataRate == 5:     # 43.4 Mbps
            return 4 * (3/4) * (52 if NSS == 1 else 108) * NSS
        elif self.dataRate == 6:     # 57.8 Mbps
            return 6 * (2/3) * (52 if NSS == 1 else 108) * NSS
        elif self.dataRate == 7:     # 65 Mbps
            return 6 * (3/4) * (52 if NSS == 1 else 108) * NSS
        else:                        # 72.2 Mbps
            return 6 * (5/6) * (52 if NSS == 1 else 108) * NSS

    def GetThroughput_UDP(self, NSS):    # Calculate total time duration for UDP
        NumberOfBitsPerSymbol = self.BitsPerSymbol(NSS)
        dataTime = self.SDur * math.ceil((self.FrameSize*8 + 6) / NumberOfBitsPerSymbol)
        Throughput = 1500*8 /(self.DIFS + \
               (self.Preamble_Normal if NSS == 1 else self.Preamble_Best) + self.RTS + self.SIFS + \
               (self.Preamble_Normal if NSS == 1 else self.Preamble_Best) + self.CTS + self.SIFS + \
               (self.Preamble_Normal if NSS == 1 else self.Preamble_Best) + dataTime + self.SIFS + \
               (self.Preamble_Normal if NSS == 1 else self.Preamble_Best) + self.ACK_MAC)
        print('Throughput (UDP) = ' + str(round(Throughput, 2)) + ' Mbps')
        print('The amount of time needed to transfer 10GB: ' + str(round(10000000 * 8 / Throughput, 2)))


    def GetThroughput_TCP(self, NSS):    # Calculate total time duration for TCP
        NumberOfBitsPerSymbol = self.BitsPerSymbol(NSS)
        dataTime = self.SDur * math.ceil((self.FrameSize*8 + 6) / NumberOfBitsPerSymbol)
        TCP_ACK_Time = self.SDur * math.ceil((self.TCP_ACK*8 + 6) / NumberOfBitsPerSymbol)
        Throughput = 1500*8 / (self.DIFS + \
               (self.Preamble_Normal if NSS == 1 else self.Preamble_Best) + self.RTS + self.SIFS + \
               (self.Preamble_Normal if NSS == 1 else self.Preamble_Best) + self.CTS + self.SIFS + \
               (self.Preamble_Normal if NSS == 1 else self.Preamble_Best) + dataTime + self.SIFS + \
               (self.Preamble_Normal if NSS == 1 else self.Preamble_Best) + self.ACK_MAC + \
               self.DIFS + \
               (self.Preamble_Normal if NSS == 1 else self.Preamble_Best) + self.RTS + self.SIFS + \
               (self.Preamble_Normal if NSS == 1 else self.Preamble_Best) + self.CTS + self.SIFS + \
               (self.Preamble_Normal if NSS == 1 else self.Preamble_Best) + TCP_ACK_Time + self.SIFS + \
               (self.Preamble_Normal if NSS == 1 else self.Preamble_Best) + self.ACK_MAC)
        print('Throughput (TCP) = ' + str(round(Throughput, 2)) + ' Mbps')
        print('The amount of time needed to transfer 10GB: ' + str(round(10000000 * 8 / Throughput, 2)))